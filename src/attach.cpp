#include <ros/ros.h>
#include "tf/transform_listener.h"
#include <sensor_msgs/LaserScan.h>
#include <string>
#include <math.h> 
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"

#include <tf/transform_listener.h>
#include "gdrone_tools/Attach.h"
#include "gdrone_tools/Center.h"

#include <std_srvs/Empty.h>
#include <std_srvs/SetBool.h>

using namespace std;
bool isAttaching;
bool isDetaching;
bool isCentering;
ros::Time callTimeHor;
ros::Time callTimeVer;
ros::Subscriber top_switch;
ros::Subscriber down_switch;
ros::Subscriber center_switch;
ros::ServiceClient attachTivaSrv;
ros::ServiceClient detachTivaSrv;
ros::ServiceClient centerTivaSrv;

bool CenterCb(gdrone_tools::Center::Request &req,
			gdrone_tools::Center::Response &res)
{
	isCentering = true;
	std_srvs::SetBool srv;
	srv.request.data = true;
	centerTivaSrv.call(srv);

	callTimeHor = ros::Time::now();
	while(isCentering)
	{
		if(ros::Time::now() - callTimeHor > ros::Duration(3))
		{
			res.result = gdrone_tools::Center::Response::TIMEOUT;
			system("rostopic pub -1 /horiz_motor_pwm std_msgs/Int8 \"data: 0\" ");

			return true;
		}		
	}
	res.result = gdrone_tools::Center::Response::SUCCESS;

	return true;
}

bool AttachCb(gdrone_tools::Attach::Request &req, 
            gdrone_tools::Attach::Response &res)
{
	std_srvs::SetBool srv;
	srv.request.data = true;
	
	callTimeVer = ros::Time::now();
	if(req.attach == 1)
	{
		isAttaching = true;
		attachTivaSrv.call(srv);
		std::cout<<"call tiva srv for attach"<<std::endl;
	}
	else
	{
		detachTivaSrv.call(srv);
		isDetaching = true;
		std::cout<<"call tiva srv for detach"<<std::endl;

	}
	
	if(isAttaching)
	{	
		while(isAttaching)
		{
			if(ros::Time::now() - callTimeVer > ros::Duration(3))
			{
				res.result = gdrone_tools::Attach::Response::TIMEOUT;
				system("rostopic pub -1 /gripper_motor_pwm std_msgs/Int8 \"data: 0\" ");
				return true;
			}		
		}
	}
	else if(isDetaching)
	{	
		while(isDetaching)
		{
			if(ros::Time::now() - callTimeVer > ros::Duration(3))
			{
				res.result = gdrone_tools::Attach::Response::TIMEOUT;
				system("rostopic pub -1 /gripper_motor_pwm std_msgs/Int8 \"data: 0\" ");
				return true;
			}		
		}
	}
	
	res.result = gdrone_tools::Attach::Response::SUCCESS;

	return true;
}

void upDetect(const std_msgs::Bool::ConstPtr& data)
{
	if(isAttaching && data->data)
	{
		isAttaching = false;
	}
}

void downDetect(const std_msgs::Bool::ConstPtr& data)
{
	if(isDetaching && data->data)
		isDetaching = false;
}

void centerDetect(const std_msgs::Bool::ConstPtr& data)
{
	if(isCentering && !data->data)
		isCentering = false;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "attach_claw");
	ros::NodeHandle n("~");
	// Resolve the scan topic where to puchish the assembled laser

	top_switch = n.subscribe<std_msgs::Bool>("/up_limit_detect", 1, upDetect);
	down_switch = n.subscribe<std_msgs::Bool>("/down_limit_detect", 1, downDetect);
	center_switch =  n.subscribe<std_msgs::Bool>("/optical_switch_detect", 1, centerDetect);
	ros::ServiceServer AttachSrv= n.advertiseService("Attach", &AttachCb);
	ros::ServiceServer CenterSrv= n.advertiseService("Center", &CenterCb);

	attachTivaSrv = n.serviceClient<std_srvs::SetBool>("/attach_pallet");
	detachTivaSrv = n.serviceClient<std_srvs::SetBool>("/detach_pallet");
	centerTivaSrv = n.serviceClient<std_srvs::SetBool>("/center_gripper");

	ros::Rate rate(60.0);
	//ros::Timer timer = nh.createTimer(ros::Duration(1)
	ros::MultiThreadedSpinner spinner(4); // Use 4 threads

	while (n.ok())
	{
		/*if(hasNewScan)
		{
			//analyseNewScan();
			hasNewScan = false;
		}*/
		spinner.spin();
		rate.sleep();  
	}


	return 0;
}
