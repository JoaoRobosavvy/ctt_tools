#include <ros/ros.h>
#include "tf/transform_listener.h"
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/OccupancyGrid.h>
//#include <laser_geometry/laser_geometry.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <string>
#include <math.h> 
#include "std_msgs/String.h"
#include <tf/transform_listener.h>
#define PI 3.14159265
#define SIZE 8000
#define RESOLUTION 0.01
#define LASER_RANGE 2.0
#define WINDOW_SIZE 100

struct area
{
	double x;
	double y;
	double w;
	double h;
	bool isFree;
	int nReading;
	void setOccupied()
	{
		nReading++;
		if(nReading > 4)
			isFree = false;
	}
	void setup(double _x, double _y, double _w, double _h)
	{
		x = _x;
		y = _y;
		w = _w;
		h = _h;
	}
	bool isInside(double _x, double _y)
	{
		if(_x > x && _x < x + w && _y > y && _y < y + h)
			return true;
		else 
			return false;
		
	}
};
ros::Publisher fake_scan_pub;


ros::Subscriber laser_sub;

sensor_msgs::LaserScan newScan;

bool to_transform = false;

tf::TransformListener *listener;
ros::Time curr_time;
std::vector<area> palletedrop;

void laserReceived(const sensor_msgs::LaserScan::ConstPtr& scanH)
{
	if(to_transform)
	{
		newScan = *scanH;
		int n_leituras = 0;
		//get_robot_pose();
		for(int i=0; i<newScan.ranges.size(); i++)
		{
			double closest_angle;

			closest_angle = ((double)(i*newScan.angle_increment) + newScan.angle_min)+3.14/2;
			double true_angle = ((double)(i*newScan.angle_increment) + newScan.angle_min);
			double true_x = newScan.ranges[i] * cos(true_angle);
			double true_y = newScan.ranges[i] * sin(true_angle);

			double x_obs = newScan.ranges[i] * cos(closest_angle);
			double y_obs = newScan.ranges[i] * sin(closest_angle);

//			if(x_obs > -0.4 && x_obs < 0.4)
	//			{

			geometry_msgs::PointStamped bll;
			bll.header.frame_id = "/agv1/base_link";
			bll.header.stamp = curr_time;
			bll.point.x = true_x;
			bll.point.y = true_y;
			bll.point.z = 0.0;
			geometry_msgs::PointStamped m;

			listener->transformPoint("map", bll,m);
			
			if(m.point.x > 23 && m.point.x < 25 && m.point.y > -4.50 && m.point.y < 4.7)
			{
				std::cout<<"Transform "<<m.point.x<<std::endl;
				for(int i = 0; i < palletedrop.size(); i++)
				{
					if(palletedrop[i].isInside(m.point.x, m.point.y))
						palletedrop[i].setOccupied();
				}

			}
			else
			{
				newScan.ranges[i] = 0;
			}
			//ROS_INFO("Obstacle at %d %d", x_obs_index, y_obs_index);
			
		}
		fake_scan_pub.publish(newScan);

	}
}



int main(int argc, char** argv)
{
	ros::init(argc, argv, "laser_adapt");
	ros::NodeHandle nh("~"), g_nh;
	std::string scanTopic;
	tf::TransformListener lr(ros::Duration(10));
	listener = &lr;
	// Resolve the scan topic where to puchish the assembled laser
	laser_sub = nh.subscribe<sensor_msgs::LaserScan>("/agv1/base_scan", 1, laserReceived);
	fake_scan_pub = g_nh.advertise<sensor_msgs::LaserScan>("/agv1/fake_base_scan", 50);
	// Loop forever
	ros::Rate rate(10.0);
	//ros::Timer timer = nh.createTimer(ros::Duration(1)
	
	double startx, starty, palletew, palleth;
	startx = 23;
	starty = 4.7;
	palletew = 0.8;
	palleth = 1;
	int npalletes = 8;
	area aux;
	for (int i = 0; i < 8; i++)
	{
		aux.setup(startx, starty - palletew*i, palletew, palleth);
		palletedrop.push_back(aux);
	}
	
	while (nh.ok())
	{
		//ros::spin();
		tf::StampedTransform transform;
		curr_time = ros::Time(0);
		try
		{
			listener->waitForTransform("agv1/base_link","map", curr_time, ros::Duration(1.5));
			listener->lookupTransform("map", "agv1/base_link", curr_time, transform);
			
			to_transform = true;
		}
		catch (tf::TransformException ex)
		{
			//ROS_ERROR("%s",ex.what());
			to_transform = false;
		}


		ros::spinOnce();
		rate.sleep();  
	}


	return 0;
}
