#include <ros/ros.h>
#include "tf/transform_listener.h"
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <string>
#include <math.h> 
#include "std_msgs/String.h"
#include <tf/transform_listener.h>
#include <gdrone_tools/GoBack.h>
#include <gdrone_tools/Dist.h>
#include <std_srvs/Empty.h>
#include <geometry_msgs/Twist.h>

#define PI 3.14159265
#define SIZE 8000
#define RESOLUTION 0.01
#define LASER_RANGE 2.0
#define WINDOW_SIZE 100
using namespace std;

struct area
{
};


ros::Subscriber laser_sub;
ros::ServiceServer service, service2;
sensor_msgs::LaserScan newScan;
ros::Publisher stopper_warn;
bool isObstacle = false;
bool isObstacleActive = true;
float av_dist;
float begin_distance;
float close_dist_l, far_dist_l, diff_l;
float close_dist_r, far_dist_r, diff_r;
float avg_r, avg_l;
bool analyse = false;
int n_leituras_back = 0;
double closest_obstacle;
ros::Time callTime;
bool _hasNewScan = false;
void laserReceived(const sensor_msgs::LaserScan::ConstPtr& scanH)
{
	newScan = *scanH;
	_hasNewScan = true;
	
	
}

void analyzeNewScan()
{
	int n_leituras = 0;
	//get_robot_pose();
	double total_dist = 0;
	close_dist_r = 1000;
	far_dist_r = 0;
	close_dist_l = 1000;
	far_dist_l = 0;
	closest_obstacle = 100;

	for(int i=0; i<newScan.ranges.size(); i++)
	{
		double closest_angle;
			
		closest_angle = ((double)(i*newScan.angle_increment) + newScan.angle_min)+3.14/2;
		double true_angle = ((double)(i*newScan.angle_increment) + newScan.angle_min);
		double true_x = newScan.ranges[i] * cos(true_angle);
		double true_y = newScan.ranges[i] * sin(true_angle);

		double x_obs = newScan.ranges[i] * cos(closest_angle);
		double y_obs = newScan.ranges[i] * sin(closest_angle);
			
      
		if((x_obs> -0.25 && x_obs < -0.1) || (x_obs > 0.1 && x_obs < 0.25))
		{
			if(y_obs < closest_obstacle)
				closest_obstacle = y_obs;//newScan.ranges[i];
		} 
    
		if(x_obs >  -.1  &&  x_obs < 0.1)
		{
			total_dist += y_obs;//newScan.ranges[i];
			n_leituras++;
		}
		else
		{
			//newScan.ranges[i] = 0;
		}
    
   
		if((x_obs > -0.15 &&  x_obs < -.1 ) )
		{
			if(y_obs < close_dist_l)
				close_dist_l = y_obs;//newScan.ranges[i];
            
			if(y_obs > far_dist_l)
			{
				far_dist_l = y_obs;//newScan.ranges[i];
			}

		}
		else if (x_obs > 0.1 && x_obs < 0.15)
		{
			if(y_obs < close_dist_r)
				close_dist_r = y_obs;//newScan.ranges[i];
            
			if(y_obs > far_dist_r)
			{
				far_dist_r = y_obs;//newScan.ranges[i];
			}
		}
    
	}
	
	diff_l = far_dist_l - close_dist_l;
	diff_r = far_dist_r - close_dist_r;

	av_dist = total_dist/n_leituras;
    //  cout<<"Diff "<<diff_l<<"  ;  "<<diff_r<<endl;
      
	if(analyse == true)
	{
		avg_l += diff_l;
		avg_r += diff_r;
		// cout<<"Junta 10"<<endl;
		n_leituras_back ++;
	}
  
	if(n_leituras_back >= 10)
	{
		analyse = false;
		n_leituras_back = 0;
		// cout<<"Ok proceed"<<endl;
	}
  
	std::ostringstream strs;
	strs << closest_obstacle;
	std::string str = strs.str();
	std_msgs::String msg;
	msg.data =  str.c_str();
  
	if(isObstacleActive)
		stopper_warn.publish(msg);
	else
	{
		msg.data = "1";
		stopper_warn.publish(msg);
	}
	//std::cout<<av_dist<<std::endl;
}

bool DistCb(gdrone_tools::Dist::Request &req, 
            gdrone_tools::Dist::Response &res)
{
	res.result = av_dist;
	// std::cout<<"From pallete seeker "<<av_dist;
	return true;
}

bool ActivateObstaclesCb(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
{
	isObstacleActive = true;
	return true;
}

bool DeactivateObstaclesCb(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
{
	isObstacleActive = false;
	return true;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "pallete_seeker");
	ros::NodeHandle nh("~"), g_nh;
	std::string scanTopic;
	
	// Resolve the scan topic where to puchish the assembled laser
	laser_sub = nh.subscribe<sensor_msgs::LaserScan>("/base_scan_back", 1, laserReceived);

	service2 = g_nh.advertiseService("Back_dist", &DistCb);
	ros::ServiceServer serviceActivate  = g_nh.advertiseService("ActivateBackObstacles", ActivateObstaclesCb);
	ros::ServiceServer serviceDeactivate  = g_nh.advertiseService("DeactivateBackObstacles", DeactivateObstaclesCb);  
	// Loop forever
	ros::MultiThreadedSpinner spinner(4); // Use 4 threads
	ros::Rate rate(60.0);

	//ros::Timer timer = nh.createTimer(ros::Duration(1)
	while (nh.ok())
	{
		//ros::spinOnce();
		
		spinner.spin(); // spin() will not return until the node has been shutdown
		if(_hasNewScan)
		{
			analyzeNewScan();
			_hasNewScan = false;
		}
		
		rate.sleep();  
	}

	return 0;
}
