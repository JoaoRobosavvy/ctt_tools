#include <ros/ros.h>
#include "tf/transform_listener.h"
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/Pose2D.h>
#include <geometry_msgs/Point.h>
#include <string>
#include <math.h> 
#include "std_msgs/String.h"
#include <tf/transform_listener.h>
#include <std_srvs/Empty.h>
#include <gdrone_tools/Area.h>
#include <geometry_msgs/Twist.h>
#include <ctt_laser_msgs/Zones.h>
#define PI 3.14159265
#define SIZE 8000
#define RESOLUTION 0.01
#define LASER_RANGE 2.0
#define WINDOW_SIZE 100

struct AreaType1
{
	float dprotection;
	float dwarn1;
	float dwarn2;
	void setup(float _dprotection, float _dwarn1, float _dwarn2)
	{
		dprotection = _dprotection;
		dwarn1 = _dwarn1;
		dwarn2 = _dwarn2;
		
		if(dwarn2 > dwarn1 && dwarn1 > dprotection)
		{
			std::cout<<"SUCESS"<<std::endl;
		}
		else
			std::cout<<"Failed AreaType1, check areas"<<std::endl;
	}
};

struct AreaType2
{
	float dx;
	float dprotection;
	float dwarn1;
	float dwarn2;
	void setup(float _dx, float _dprotection, float _dwarn1, float _dwarn2)
	{
		dx = _dx;
		dprotection = _dprotection;
		dwarn1 = _dwarn1;
		dwarn2 = _dwarn2;
		
		if(dwarn2 > dwarn1 && dwarn1 > dprotection)
		{
			std::cout<<"SUCESS"<<std::endl;
		}
		else
			std::cout<<"Failed AreaType2, check areas"<<std::endl;
	}
};

struct AreaType3
{
	float dx1, dx2, dprotection;
	
	void setup(float _dx1, float _dx2, float _dprotection)
	{
		dprotection = _dprotection;
		dx1 = _dx1;
		dx2 = _dx2;
		
		if(dx2 > dx1 )
		{
			std::cout<<"SUCESS"<<std::endl;
		}
		else
			std::cout<<"Failed AreaType3, check areas"<<std::endl;
	}
};

ros::Subscriber laser_sub;
ros::Publisher stopper_warn, block_warn;
ros::ServiceServer Areasrv;
sensor_msgs::LaserScan newScan;
int n_obs = 0;
int AreaID = 1;
tf::TransformListener *listener;
ros::Time curr_time;
ctt_laser_msgs::Zones zonemsg;

double closest_obstacle;
bool isObstacleActive;
bool hasNewScan = false;

AreaType1 area1;
AreaType2 area2, area3;
AreaType3 area4;

void laserReceived(const sensor_msgs::LaserScan::ConstPtr& scanH)
{
	newScan = *scanH;
	hasNewScan = true;
}

bool analyseNewScan()
{
	ctt_laser_msgs::Zones msg;
	int n_leituras = 0;
	//get_robot_pose();
	double closest_obstacle = 100;
	
	msg.protection = false;
	msg.warn1 = false;
	msg.warn2 = false;
	for(int i=0; i<newScan.ranges.size(); i++)
	{
		double closest_angle;
			
		closest_angle = ((double)(i*newScan.angle_increment) + newScan.angle_min)+3.14/2;
		double x_obs = newScan.ranges[i] * cos(closest_angle);
		double y_obs = newScan.ranges[i] * sin(closest_angle);
			
		if(AreaID == 1)
		{
			if(newScan.ranges[i] <= area1.dprotection)
				msg.protection = true;
				
			if(newScan.ranges[i] <= area1.dwarn1 && newScan.ranges[i] > area1.dprotection)
				msg.warn1 = true;
				
			if(newScan.ranges[i] <= area1.dwarn2 && newScan.ranges[i] > area1.dwarn1)
				msg.warn2 = true;
		}
		else if(AreaID == 2)
		{
			if(x_obs > -area2.dx && x_obs < area2.dx)
			{
				if(y_obs <= area2.dprotection)
					msg.protection = true;
				
				if(y_obs <= area2.dwarn1 && y_obs > area2.dprotection)
					msg.warn1 = true;
				
				if(y_obs <= area2.dwarn2 && y_obs > area2.dwarn1)
					msg.warn2 = true;
			}
		}
		else if(AreaID == 3)
		{
			if(x_obs > -area3.dx && x_obs < area3.dx)
			{
				if(y_obs <= area3.dprotection)
					msg.protection = true;
				
				if(y_obs <= area3.dwarn1 && y_obs > area3.dprotection)
					msg.warn1 = true;
				
				if(y_obs <= area3.dwarn2 && y_obs > area3.dwarn1)
					msg.warn2 = true;
			}
		}
		else if(AreaID == 4)
		{
			if((x_obs > -area4.dx2 && x_obs < -area4.dx1) || (x_obs > area4.dx1 && x_obs < area4.dx2))
			{
				if(y_obs < area4.dprotection)
					msg.protection = true;
			}
		}
	}
	stopper_warn.publish(msg);
	
	/*std::cout<<closest_obstacle<<std::endl;
	std::ostringstream strs;
	strs << closest_obstacle;
	std::string str = strs.str();
	//if(n_leituras > 0)
	//{
	std_msgs::String msg;
	msg.data =  str.c_str();
	
  if(isObstacleActive)
    stopper_warn.publish(msg);
  else
  {
    msg.data = "1";
    stopper_warn.publish(msg);
  }
  
  std_msgs::String obs_msg;

  if(closest_obstacle < 1.1)
  {
    obs_msg.data = "Blocked";
  }
  else
  {
    obs_msg.data = "Move";
  }
  
  if(isObstacleActive)
    block_warn.publish(obs_msg);
    */
  hasNewScan = false;
}
bool ActivateObstaclesCb(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
{
  isObstacleActive = true;
  return true;
}

bool DeactivateObstaclesCb(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
{
  isObstacleActive = false;
  return true;
}

bool ChangeAreaCb(gdrone_tools::Area::Request& request, gdrone_tools::Area::Response& response)
{
	AreaID = request.AreaPattern;
	std::cout<<"Change to "<<AreaID<<std::endl;
	return true;
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "obstacle_stop");
	ros::NodeHandle nh("~"), g_nh;
	std::string scanTopic;
	tf::TransformListener lr(ros::Duration(10));
	listener = &lr;
	// Resolve the scan topic where to puchish the assembled laser
	laser_sub = nh.subscribe<sensor_msgs::LaserScan>("/agv1/base_scan", 1, laserReceived);
	stopper_warn = g_nh.advertise<ctt_laser_msgs::Zones>("obstacleFactor", 1000);
	ros::ServiceServer serviceActivate  = g_nh.advertiseService("ActivateObstacles", ActivateObstaclesCb);
	ros::ServiceServer serviceDeactivate  = g_nh.advertiseService("DeactivateObstacles", DeactivateObstaclesCb);  
	ros::ServiceServer serviceArea = g_nh.advertiseService("ChangeArea", &ChangeAreaCb);
	
	area1.setup(0.5,1.0,1.5);
	area2.setup(.33, .5, 1.0, 1.5);
	area3.setup(.25, .5, 1.0, 1.5);
	 
	isObstacleActive = true;
	
	ros::Rate rate(60.0);
	//ros::Timer timer = nh.createTimer(ros::Duration(1)
	while (nh.ok())
	{
		//ros::spin();
		tf::StampedTransform transform;
		curr_time = ros::Time(0);
		
		if(hasNewScan && isObstacleActive)
		{
			analyseNewScan();
		}
		
		ros::spinOnce();
		rate.sleep();  
	}


	return 0;
}
