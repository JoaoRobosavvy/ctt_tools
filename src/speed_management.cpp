#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include "std_msgs/String.h"
#include "ctt_manager_msgs/InterfaceLog.h"
#include "ctt_manager_msgs/InterfaceAlert.h"
using namespace std;
ros::Publisher  alert_pub, log_pub;

ros::Subscriber velocity_sub, obstacle_sub, geofence_sub, velocityrc_sub;
double speed_x, speed_teta;
geometry_msgs::Twist twist;
ros::Publisher vel_pub_, led_manager;
double obs_factor, fence_factor, back_obs_factor;
double min_obs_dist, max_obs_dist, min_geo_dist, max_geo_dist;
bool isBlocked = false;
int stateLed = 0;

void change_Leds(std::string color)
{
  std_msgs::String msg;
  if(color == "red")
  {
    //msg.data 
    msg.data = "fr0001";
  }
  else if(color == "green")
  {
    msg.data  = "fg0001";
  }
  else if(color == "blue")
  {
    msg.data = "fb0001";
  }
  else if(color == "blinkl")
  {
    msg.data = "lo0200";

  }
  else if(color == "blinkr")
  {
    msg.data = "ro0200";

  }
  
  led_manager.publish(msg);
}

void sendAlert(int iD, int level, std::string source, std::string description, std::string required)
{
	ctt_manager_msgs::InterfaceAlert msg;
	msg.stamp = ros::Time::now();
	msg.id = iD;
	msg.source = source;
	msg.description = description,
	msg.required = required;
	msg.level = level;
	alert_pub.publish(msg);
}

void sendLog(int level, std::string source, std::string description)
{
	ctt_manager_msgs::InterfaceLog msg;
	msg.stamp = ros::Time::now();
	msg.source = source;
	msg.description = description,
	msg.level = level;
	log_pub.publish(msg);
	
}

float ofMap(float value, float inputMin, float inputMax, float outputMin, float outputMax, bool clamp) {

	if (fabs(inputMin - inputMax) < FLT_EPSILON)
	{
		return outputMin;
	} 
	else 
	{
		float outVal = ((value - inputMin) / (inputMax - inputMin) * (outputMax - outputMin) + outputMin);
	
		if( clamp )
		{
			if(outputMax < outputMin){
				if( outVal < outputMax )
					outVal = outputMax;
				else if( outVal > outputMin)
					outVal = outputMin;
			}
			else
			{
				if( outVal > outputMax)
					outVal = outputMax;
				else if( outVal < outputMin )
					outVal = outputMin;
			}
		}
		return outVal;
	}
}

void obstacleCb(const std_msgs::String::ConstPtr& msg)
{
	double val = atof(msg->data.c_str());
	//if(val < 1)
	//	obs_factor = 0;
	//else if(val >= 1 && val < 3)
	//{
	obs_factor = ofMap(val,0.2,1,0,1,true);

	//}
	//else
		//obs_factor = 1;
}

void backobstacleCb(const std_msgs::String::ConstPtr& msg)
{
	double val = atof(msg->data.c_str());
	//if(val < 1)
	//	obs_factor = 0;
	//else if(val >= 1 && val < 3)
	//{
	back_obs_factor = ofMap(val,0.2,1,0,1,true);

	//}
	//else
		//obs_factor = 1;
}

void geofenceCb(const std_msgs::String::ConstPtr& msg)
{
	double val = atof(msg->data.c_str());
	/*if(val == 0 )
		fence_factor = val;
	else 
		fence_factor = ofMap(val, 0.2, 1, 0, 1, true);*/
    if(val < 0.13)
		fence_factor = 0;
    else
		fence_factor = 1;
}
  
void velCallback(const geometry_msgs::Twist::ConstPtr& cmd_vel)
{
	float factor;
	bool isfencing = false;
	if(obs_factor < fence_factor)
	{
		factor = obs_factor;
		if(!isBlocked)
		{
			isBlocked = true;
			sendAlert(10, 2, "agv1", "Has obstacle on path", "remove obstacle");
			change_Leds("red");
		}
	}
	else if(obs_factor > fence_factor)
	{
		isfencing = true;
		factor = fence_factor;
		if(!isBlocked)
		{
			isBlocked = true;
			sendAlert(10, 2, "agv1", "Has obstacle on path", "remove obstacle");
			change_Leds("red");
		}
	}
	else if(obs_factor == 1 && fence_factor == 1)
	{
		if(isBlocked)
		{
			isBlocked = false;
			sendAlert(10, 0, "agv1", "Obstacle was removed", "");
			change_Leds("blue");
		}
	}
	
	if(cmd_vel->linear.x > 0 )
		speed_x = cmd_vel->linear.x * obs_factor;
	else if(cmd_vel->linear.x < 0 && isfencing)
  		speed_x = cmd_vel->linear.x * fence_factor;
	else
		speed_x = cmd_vel->linear.x;
		
	speed_teta = cmd_vel->angular.z;
	twist.angular.z = speed_teta;
	twist.linear.x = speed_x;
  
 /* if(twist.angular.z > 0 && !isBlocked)
  {
    if(stateLed != 1)
    {
      stateLed = 1;
      change_Leds("blinkl");
    }
  }
  else if(twist.angular.z < 0 && !isBlocked)
  {
    if(stateLed != 2)
    {
      stateLed = 2;
      change_Leds("blinkr");
    }  
  }
  else if(twist.angular.z == 0 && !isBlocked)
  {
    if(stateLed != 0 )
    {
      stateLed = 0;
      change_Leds("green");
      
    }    
  }*/
  
 /* if(isBlocked)
  {
    if(stateLed != 3)
    {
      stateLed = 3;
      change_Leds("red");
    }
  }*/
  
	
  
  //cout<<"Factors: obstacle "<<obs_factor<<" ; "<<fence_factor<<endl;
	//ROS_INFO("linear: %f  ; angular: %f", twist.linear.x, twist.angular.z);
	vel_pub_.publish(twist);
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "speed_manager");
	ros::NodeHandle nh("~");
	
	velocity_sub = nh.subscribe<geometry_msgs::Twist>("/cmd_vel_smooth", 1, velCallback);

	ros::Subscriber obstacle_sub = nh.subscribe("/obstacleFactor", 1000, obstacleCb);
	ros::Subscriber geofence_sub = nh.subscribe("/geofenceFactor", 1000, geofenceCb);
	ros::Subscriber back_obstacle_sub = nh.subscribe("/backobstacleFactor", 1000, backobstacleCb);
	alert_pub = nh.advertise<ctt_manager_msgs::InterfaceAlert>("/interface/alerts", 1);
	log_pub = nh.advertise<ctt_manager_msgs::InterfaceLog>("/interface/agv_logs", 1);
  
	nh.param("min_geo_dist",      min_geo_dist,  1.0);
	nh.param("max_geo_dist",      max_geo_dist,  2.0);
	nh.param("min_obs_dist",      min_obs_dist,  1.0);
	nh.param("max_obs_dist",      max_obs_dist,  2.0);
	vel_pub_ = nh.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
    led_manager = nh.advertise<std_msgs::String>("/effects", 1);

	fence_factor = 1;
	obs_factor = 1;
	back_obs_factor = 1;

	ros::spin();
}


