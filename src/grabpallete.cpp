#include <ros/ros.h>
#include "tf/transform_listener.h"
#include <sensor_msgs/LaserScan.h>
#include <string>
#include <math.h> 
#include "std_msgs/String.h"
#include <tf/transform_listener.h>
#include "gdrone_tools/Dist.h"
#include <std_srvs/Empty.h>
#include "ctt_agv_srvs/Align.h"
#include <std_srvs/Trigger.h>

#define PI 3.14159265
using namespace std;
struct BehaviourTree
{
 map <int, string> state;
 int currState;
 /* = {
  { "ROTATING TO RAIL", 0},
  { "MEASURING RAIL", 1 },
  { "ROTATING TO ALIGN", 2 },
  { "ALIGNING", 3 },
  { "FETCHING", 4 },
  { "VERIFY PALLETE", 5 },
  { "DOCK", 6 },
  { "EXIT RAIL", 7 },
  { "WAITING", 8 }
  };*/
	/*
	 * 0 - "ROTATING"
	 * 1 - "MEASURING"
	 * 2 - "ROTATING_ALIGN"
	 * 3 - "ALIGNING"
	 * 4 - "FETCHING"
	 * 5 - "VERIFY"
	 * 6 - "DOCK"
	 * 7 - "EXIT"
	 * 8 - "WAITING"
	 * 
	 */
	void create()
	{
		state.insert(std::make_pair(0, "ROTATING"));
		state.insert(std::make_pair(1, "MEASURING"));
		state.insert(std::make_pair(2, "ROTATING_ALIGN"));
		state.insert(std::make_pair(3, "ALIGNING"));
		state.insert(std::make_pair(4, "FETCHING"));
		state.insert(std::make_pair(5, "VERIFY"));
		state.insert(std::make_pair(6, "DOCK"));
		state.insert(std::make_pair(7, "EXIT"));
		state.insert(std::make_pair(8, "WAITING"));
		currState = 8;

	}
	void startTree()
	{
		//state = 0;
		currState = 0;
	}
	
	string getState()
	{
		return state[currState];
	}
	
	void changeState()
	{
		currState++;
		cout<<"Change to "<<getState()<<endl;
	}
};

BehaviourTree tree;
ros::ServiceClient check_pallete_srv; 
ros::ServiceClient measure_back_srv; 
ros::ServiceClient measure_front_srv;
ros::ServiceClient align_srv;
ros::ServiceClient dock_srv;

double x, y, angle_robot, start_dist;
double rail_angle = 1.57079632679;
bool to_transform;
tf::TransformListener* listener;
ros::Publisher twist_pub;

bool GrabPalleteCb(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
{
	int bla = 0;
	tree.startTree();
	return true;
}

void updateRobotPosition()
{
    tf::StampedTransform transform;
	try
	{
		listener->waitForTransform("/agv1/base_link", "map", ros::Time(0), ros::Duration(0.1));
		listener->lookupTransform("map", "/agv1/base_link", ros::Time(0), transform);
		x = transform.getOrigin().x();
		y = transform.getOrigin().y();
		angle_robot = tf::getYaw(transform.getRotation());
		to_transform = true;
	}
	catch (tf::TransformException ex)
	{
		ROS_ERROR("%s", ex.what());
		to_transform = false;
	}
}

void stopRobot()
{
	geometry_msgs::Twist twist;
    twist.linear.x = 0;
    twist.linear.y = 0;
    twist.angular.z = 0.0;
    twist_pub.publish(twist);
}

void rotate(double angle)
{
	double dteta = angle_robot - angle;
    geometry_msgs::Twist twist;
    twist.linear.x = 0;
    twist.linear.y = 0;
    if (dteta < -3.14 || dteta > 0.05)
    {
		twist.angular.z = -0.3;
		twist_pub.publish(twist);
    }
    else if (dteta < -0.05 && dteta > -3.14)
    {
		twist.angular.z = 0.3;
		twist_pub.publish(twist);
	}
    else if (dteta < 0.05 && dteta > -0.05)
    {
		stopRobot();
		tree.changeState();
	}
}

 //! Drive forward a specified distance based on odometry information
bool driveForwardOdom(double distance)
{
	//wait for the listener to get the first message
	listener->waitForTransform("/agv1/base_link", "/agv1/odom", ros::Time(0), ros::Duration(1.0));
     
	//we will record transforms here
	tf::StampedTransform start_transform;
	tf::StampedTransform current_transform;
 
	//record the starting transform from the odometry to the base frame
	listener->lookupTransform("/agv1/base_link", "/agv1/odom", ros::Time(0), start_transform);
     
	//we will be sending commands of type "twist"
	geometry_msgs::Twist base_cmd;
	//the command will be to go forward at 0.25 m/s
	base_cmd.linear.y = base_cmd.angular.z = 0;
	base_cmd.linear.x = 0.25;
	ros::Rate rate(10.0);
	bool done = false;
	while (!done )
	{
		//send the drive command
		twist_pub.publish(base_cmd);
		rate.sleep();
		//get the current transform
		try
		{
			listener->lookupTransform("/agv1/base_link", "/agv1/odom", ros::Time(0), current_transform);
		}
		catch (tf::TransformException ex)
		{
			ROS_ERROR("%s",ex.what());
			break;
		}
		//see how far we've traveled
		tf::Transform relative_transform = start_transform.inverse() * current_transform;
		double dist_moved = relative_transform.getOrigin().length();

		if(dist_moved > distance) 
			done = true;
	}
	if (done) 
		return true;
	return false;
}

void drive(bool _forward)
{
	int factor;
	if(_forward)
		factor = 1;
	else
		factor = -1;
		
	geometry_msgs::Twist twist;
    twist.linear.x = 0.5 * factor;
    twist.linear.y = 0;
    twist_pub.publish(twist);
}


int main(int argc, char** argv)
{
	ros::init(argc, argv, "grab_pallete");

	tf::TransformListener lr(ros::Duration(10));
    listener = &lr;
	ros::NodeHandle n("~");
	check_pallete_srv = n.serviceClient<gdrone_tools::Dist>("/back/BackwardsPallete");
    measure_back_srv = n.serviceClient<gdrone_tools::Dist>("/back/MeasurePallete");
    measure_front_srv = n.serviceClient<gdrone_tools::Dist>("/front/MeasurePallete");
    dock_srv = n.serviceClient<std_srvs::Trigger>("/agv1/attach");

    align_srv = n.serviceClient<ctt_agv_srvs::Align>("/align");

	ros::ServiceServer grabPallete  = n.advertiseService("GrabPallete", GrabPalleteCb);

	twist_pub = n.advertise<geometry_msgs::Twist>("/agv1/cmd_vel", 1);

	tree.create();
	ros::Rate rate(10.0);
	//ros::Timer timer = nh.createTimer(ros::Duration(1)
	while (n.ok())
	{
		updateRobotPosition();
		
		if(tree.getState() == "ROTATING")
		{
			rotate(-rail_angle);
		}
		else if(tree.getState() == "MEASURING")
		{
			gdrone_tools::Dist srv;
			measure_front_srv.call(srv);
			cout<<srv.response<<endl;
			tree.changeState();
		}
		else if(tree.getState() == "ROTATING_ALIGN")
		{
			rotate(rail_angle);
		}
		else if(tree.getState() == "ALIGNING")
		{
			ctt_agv_srvs::Align srv;
			srv.request.timeout = 50;
			srv.request.row = 2;
			srv.request.forward = false;
			srv.request.align_type = 0;
			cout<<"align response "<<srv.response.result<<endl;
			align_srv.call(srv);
			gdrone_tools::Dist srv2;
			measure_back_srv.call(srv2);
			start_dist = srv2.response.result;
			tree.changeState();
			
		}
		else if(tree.getState() == "FETCHING")
		{
			gdrone_tools::Dist srv;
			measure_back_srv.call(srv);
			cout<<srv.response.result<<endl;
			if(srv.response.result > 0.4)
				drive(false);
			else
			{
				stopRobot();
				tree.changeState();
			}
		}
		else if(tree.getState() == "VERIFY")
		{
			gdrone_tools::Dist srv;
			check_pallete_srv.call(srv);
			if(srv.response.result == 1)
				cout<<"ISSUE WITH PALLETE"<<endl;
			else
				tree.changeState();
		}
		else if(tree.getState() == "DOCK")
		{
			gdrone_tools::Dist srv;
			measure_back_srv.call(srv);
			if(srv.response.result > 0.19)
				drive(false);
			else
			{
				stopRobot();
				std_srvs::Trigger srv2;
				dock_srv.call(srv2);
				cout<<"DOCK"<<endl;
				tree.changeState();
			}
		}
		else if(tree.getState() == "EXIT")
		{
			driveForwardOdom(start_dist - 0.5);
			stopRobot();
			tree.changeState();
		}
		else if(tree.getState() == "WAITING")
		{
			cout<<"IM DONE"<<endl;
		}
		ros::spinOnce();
		rate.sleep();  
	}


	return 0;
}
