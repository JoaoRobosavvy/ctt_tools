#include <ros/ros.h>
#include <math.h>
using namespace std;
#include <algorithm>
#include <vector>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/Odometry.h>
#include "std_msgs/String.h"
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Point.h>
#include <std_srvs/Empty.h>
typedef double coord2_t;  // must be big enough to hold 2*max(|coordinate|)^2

float ofMap(float value, float inputMin, float inputMax, float outputMin, float outputMax, bool clamp) {

	if (fabs(inputMin - inputMax) < FLT_EPSILON)
	{
		return outputMin;
	} 
	else 
	{
		float outVal = ((value - inputMin) / (inputMax - inputMin) * (outputMax - outputMin) + outputMin);
	
		if( clamp )
		{
			if(outputMax < outputMin){
				if( outVal < outputMax )
					outVal = outputMax;
				else if( outVal > outputMin)
					outVal = outputMin;
			}
			else
			{
				if( outVal > outputMax)
					outVal = outputMax;
				else if( outVal < outputMin )
					outVal = outputMin;
			}
		}
		return outVal;
	}
}

class Point
{
	public:
	Point(){};
	Point(double _x, double _y)
	{ 
		x = _x;
		y = _y;
	} 
    double x, y;
    bool operator <(const Point &p) const {
	return x < p.x || (x == p.x && y < p.y);}
};

class Line
{
	public:
    Line(const Point &p1, const Point &p2) : p1(p1), p2(p2) {}
	Point p1;
    Point p2;
 
    void setPoints(const Point &p1, const Point &p2)
    {
		this->p1 = p1;
        this->p2 = p2;
    }
};

class geoFence
{
	public:
	geoFence():
	nh_()
	{
		point_ = nh_.subscribe("/clicked_point",10,&geoFence::pointCb, this);
		//position_odom = nh_.subscribe("/odom", 10, &geoFence::odomCb, this);
		position_map = nh_.subscribe("/map_position", 10, &geoFence::mapCb, this);
		hasFence = false;
		geofencing_warn = nh_.advertise<std_msgs::String>("geofenceFactor", 1000);
		marker_pub = nh_.advertise<visualization_msgs::Marker>("path_tag", 1);
		line_strip.header.frame_id  = "/map";
		// Set time stamp
		line_strip.header.stamp  = ros::Time::now();
		// Set namespace
		line_strip.id=1;
		// Set type

		line_strip.type = visualization_msgs::Marker::LINE_STRIP;
		line_strip.scale.x = 0.01;
		line_strip.color.r=1.0;
		line_strip.color.a=1.0;
		service = nh_.advertiseService("BuildFence", &geoFence::buildFenceCb, this);
	};
	
	int isInside( Point P, vector<Point> V );
	double howCloseAmI(Point p);
	void addPoint(float x, float y);
	void addPoint(Point p);
	bool buildFenceCb(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response);
	bool buildFence();
	bool isRobotInside(Point p);
	bool hasFence;
		
	vector<Point> points;
	vector<Point> convexpoints;
	static double get_clockwise_angle(const Point &p);
	static int get_quadrant (const Point &p);
	static bool compare_points(const Point &a, const Point &b);

	private:
	int closestEdge;
	ros::Subscriber point_, position_odom, position_map;
    ros::NodeHandle nh_;
    ros::Publisher geofencing_warn;
	ros::ServiceServer service; 
	Point position;
	void pointCb(const geometry_msgs::PointStampedConstPtr& point);
	void odomCb(const nav_msgs::Odometry::ConstPtr& odom_value);
	void mapCb(const geometry_msgs::Point::ConstPtr& position_value);
	Point compute2DPolygonCentroid(vector<Point> vertices);

	vector<Line> edges;
	coord2_t cross(const Point &O, const Point &A, const Point &B);
	vector<Point> convex_hull(vector<Point> P);
	double distance_to_Line(Line edge, Point point);
	ros::Publisher marker_pub;
	visualization_msgs::Marker line_strip;
};

void geoFence::mapCb(const geometry_msgs::Point::ConstPtr& position_value)
{
	position.x = position_value->x;
	position.y = position_value->y;
	float factor = howCloseAmI(position);
	
	if(hasFence)
	{
		if(!isRobotInside(position))
		{
			factor = 0;
		}
		
		std::ostringstream strs;
		strs <<factor;
		std::string str = strs.str();
	
		std_msgs::String msg;
		msg.data =  str.c_str();
		geofencing_warn.publish(msg);
	}
}

void geoFence::odomCb(const nav_msgs::Odometry::ConstPtr& odom_value)
{
	position.x = odom_value->pose.pose.position.x;
	position.y = odom_value->pose.pose.position.y;
	float factor = howCloseAmI(position);
	
	if(hasFence)
	{
		if(!isRobotInside(position))
		{
			factor = 0;
		}
	}
	else
		factor = 1;
    
	std::ostringstream strs;
	strs <<factor;
	std::string str = strs.str();
	
	std_msgs::String msg;
	msg.data =  str.c_str();
	geofencing_warn.publish(msg);
}

void geoFence::pointCb(const geometry_msgs::PointStampedConstPtr& point)
{
	addPoint(point->point.x, point->point.y);

}

void geoFence::addPoint(Point p)
{
	points.push_back(p);
}

void geoFence::addPoint(float x, float y)
{
	points.push_back(Point(x,y));
}

Point geoFence::compute2DPolygonCentroid(vector<Point> vertices)
{
    Point centroid = Point(0, 0);
    double signedArea = 0.0;
    double x0 = 0.0; // Current vertex X
    double y0 = 0.0; // Current vertex Y
    double x1 = 0.0; // Next vertex X
    double y1 = 0.0; // Next vertex Y
    double a = 0.0;  // Partial signed area

    // For all vertices except last
    int i=0;
    for (i=0; i<vertices.size()-1; ++i)
    {
        x0 = vertices[i].x;
        y0 = vertices[i].y;
        x1 = vertices[i+1].x;
        y1 = vertices[i+1].y;
        a = x0*y1 - x1*y0;
        signedArea += a;
        centroid.x += (x0 + x1)*a;
        centroid.y += (y0 + y1)*a;
    }

    // Do last vertex separately to avoid performing an expensive
    // modulus operation in each iteration.
    x0 = vertices[i].x;
    y0 = vertices[i].y;
    x1 = vertices[0].x;
    y1 = vertices[0].y;
    a = x0*y1 - x1*y0;
    signedArea += a;
    centroid.x += (x0 + x1)*a;
    centroid.y += (y0 + y1)*a;

    signedArea *= 0.5;
    centroid.x /= (6.0*signedArea);
    centroid.y /= (6.0*signedArea);
	
    return centroid;
}


bool geoFence::buildFence()
{
	cout<<"Build it"<<endl;
	if(points.size() < 2)
		return false;
	else
	{
		/*convexpoints = convex_hull(points);
		Point centroid = compute2DPolygonCentroid(convexpoints);
		for(int i = 0; */
		//points = aux;
		//std::sort(points.begin(), points.end(), compare_points);
	}
	
	for(int i = 0; i < points.size()-1; i++)
	{
		geometry_msgs::Point p;
		p.x = points[i].x; 
		p.y = points[i].y;
		p.z = 0;
		line_strip.points.push_back(p);
		edges.push_back(Line(points[i], points[i+1]));
	}
	
	edges.push_back(Line(points[points.size()-1], points[0]));
	geometry_msgs::Point p;
	p.x = points[points.size()-1].x; 
	p.y = points[points.size()-1].y;
	p.z = 0;
	line_strip.points.push_back(p);
	p.x = points[0].x; 
	p.y = points[0].y;
	p.z = 0;
	line_strip.points.push_back(p);
	marker_pub.publish(line_strip);
	
	hasFence = true;
	return true;
}

bool geoFence::buildFenceCb(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
{
	return buildFence();
}

bool geoFence::isRobotInside(Point p)
{
	if(isInside(p, points) == 1)
		return true;
	else
		return false;
}

// 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
// Returns a positive value, if OAB makes a counter-clockwise turn,
// negative for clockwise turn, and zero if the points are collinear.
coord2_t geoFence::cross(const Point &O, const Point &A, const Point &B)
{
	return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}

double geoFence::get_clockwise_angle(const Point &p)
{   
    double angle = 0.0;
    int quadrant = get_quadrant(p);

    /*making sure the quadrants are correct*/
   // std::cout << "Point: " << p.x <<" ; " << p.y << " is on the " << quadrant << " quadrant" <<  std::endl;

    /*calculate angle and return it*/
    angle = -atan2(p.x,-p.y);
    return angle;
}

/* get quadrant from 12 o'clock*/
int geoFence::get_quadrant (const Point &p)
{
    int result = 4; //origin

    if (p.x > 0 && p.y > 0)
        return 1;
    else if(p.x < 0 && p.y > 0)
        return 2;
    else if(p.x < 0 && p.y < 0)
        return 3;
    //else 4th quadrant
    return result;
}

bool geoFence::compare_points(const Point &a, const Point &b)
{
    return (get_clockwise_angle(a) < get_clockwise_angle(b));
}

// Returns a list of points on the convex hull in counter-clockwise order.
// Note: the last point in the returned list is the same as the first one.
vector<Point> geoFence::convex_hull(vector<Point> P)
{
	int n = P.size(), k = 0;
	vector<Point> H(2*n);

	// Sort points lexicographically
	sort(P.begin(), P.end());

	// Build lower hull
	for (int i = 0; i < n; ++i) 
	{
		while (k >= 2 && cross(H[k-2], H[k-1], P[i]) <= 0) 
			k--;
		
		H[k++] = P[i];
	}

	// Build upper hull
	for (int i = n-2, t = k+1; i >= 0; i--) 
	{
		while (k >= t && cross(H[k-2], H[k-1], P[i]) <= 0) 
			k--;
		H[k++] = P[i];
	}

	H.resize(k-1);
	return H;
}

double geoFence::distance_to_Line(Line edge, Point point)
{
	double normalLength = hypot(edge.p2.x - edge.p1.x, edge.p2.y - edge.p1.y);
	double distance = (double)((point.x - edge.p1.x) * (edge.p2.y - edge.p1.y) - (point.y - edge.p1.y) * (edge.p2.x - edge.p1.x)) / normalLength;
	return distance;
}

int geoFence::isInside( Point P, vector<Point> V )
{	
    int    cn = 0;    // the  crossing number counter

    // loop through all edges of the polygon
    for (int i=0; i<V.size(); i++) {    // edge from V[i]  to V[i+1]
       if (((V[i].y <= P.y) && (V[i+1].y > P.y))     // an upward crossing
        || ((V[i].y > P.y) && (V[i+1].y <=  P.y))) { // a downward crossing
            // compute  the actual edge-ray intersect x-coordinate
            float vt = (float)(P.y  - V[i].y) / (V[i+1].y - V[i].y);
            if (P.x <  V[i].x + vt * (V[i+1].x - V[i].x)) // P.x < intersect
                 ++cn;   // a valid crossing of y=P.y right of P.x
        }
    }
    
    return (cn&1);    // 0 if even (out), and 1 if  odd (in)
}

double geoFence::howCloseAmI(Point p)
{
	double closest = 100000;
	for(int i = 0; i < edges.size(); i++)
	{
		float distance = abs(distance_to_Line(edges[i], p));
		if(distance < closest)
		{
			closestEdge = i;
			closest = distance;
		}
	}
	return closest;//ofMap(closest, 0.2,2,0,1,true);
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "geo_fencing");
	
	geoFence geofence;
	ros::spin();
	/* std::vector <Point> points;
    points.push_back( Point( 1, 3 ) );
    points.push_back( Point( 2, 1 ) );
    points.push_back( Point( -3, 2 ) );
    points.push_back( Point( -1, -1 ) );

    std::cout << "\nBefore sorting" << std::endl;
    for (int i = 0; i < points.size(); ++i)
    {
        std::cout << points[i].x <<" ; "<<points[i].y<< std::endl;
    }
    std::sort(points.begin(), points.end(), geoFence::compare_points);

    std::cout << "\nAfter sorting" << endl;
    for (int i = 0; i < points.size(); ++i)
    {
        std::cout << points[i].x <<" ; "<<points[i].y<< std::endl;
    }*/
	return 0;
	
}
