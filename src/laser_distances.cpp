#include <ros/ros.h>
#include "tf/transform_listener.h"
#include <sensor_msgs/LaserScan.h>
#include <string>
#include <math.h> 
#include "std_msgs/String.h"
#include <tf/transform_listener.h>
#include "gdrone_tools/Dist.h"
#include <std_srvs/Empty.h>

#define PI 3.14159265
#define SIZE 8000
#define RESOLUTION 0.01
#define LASER_RANGE 2.0
#define WINDOW_SIZE 100
using namespace std;

ros::Subscriber laser_sub;
sensor_msgs::LaserScan newScan;
ros::Time curr_time;
bool hasMeasurement = false;
bool _MeasurePallete = false;
bool _BackwardsPallete = false;
bool _ClosestPoint = false;
bool hasNewScan = false;

float measurement;
double closest_obstacle;
float avg_l = 0;
float avg_r = 0;
float counter = 0;

bool analyseNewScan()
{
	closest_obstacle = 1000;
	float close_dist_r = 1000;
	float far_dist_r = 0;
	float close_dist_l = 1000;
	float far_dist_l = 0;
	float total_dist = 0;
	for(int i=0; i<newScan.ranges.size(); i++)
	{

		double closest_angle = ((double)(i*newScan.angle_increment) + newScan.angle_min)+3.14/2;
		double x_obs = newScan.ranges[i] * cos(closest_angle);
		double y_obs = newScan.ranges[i] * sin(closest_angle);
			
		if(_ClosestPoint)
		{
			if((x_obs> -0.25 && x_obs < -0.1) || (x_obs > 0.1 && x_obs < 0.25))
			{
				if(y_obs < closest_obstacle)
					closest_obstacle = y_obs;
			}
		} 
		
		if(_MeasurePallete)
		{
			if(x_obs >  -.1  &&  x_obs < 0.1)
			{
				total_dist += y_obs;//newScan.ranges[i];
				counter++;
			}
		}
    
		if(_BackwardsPallete)
		{
			if((x_obs > -0.15 &&  x_obs < -.1 ) )
			{
				if(y_obs < close_dist_l)
					close_dist_l = y_obs;
            
				if(y_obs > far_dist_l)
					far_dist_l = y_obs;

			}
			else if (x_obs > 0.1 && x_obs < 0.15)
			{
				if(y_obs < close_dist_r)
					close_dist_r = y_obs;
            
				if(y_obs > far_dist_r)
				{
					far_dist_r = y_obs;
				}
			}
		}
	}
	
	if(_MeasurePallete)
	{
		measurement = float(total_dist) / (float)counter;
		hasMeasurement = true;
		counter = 0;
	}
	
	if(_BackwardsPallete)
	{
		float diff_l = far_dist_l - close_dist_l;
		float diff_r = far_dist_r - close_dist_r;
		
		avg_l += diff_l;
		avg_r += diff_r;
		counter ++;
		
		if(counter == 10)
		{
			avg_l /= 10.0;
			avg_r /= 10.0;
			if(avg_l > 0.041 || avg_r > 0.041)
				measurement = 0;
			else
				measurement = 1;
			
			counter = 0;
			hasMeasurement = true;
			avg_l = 0;
			avg_r = 0;
		}
	}
	
	
	if(_ClosestPoint)
	{
		measurement = closest_obstacle;
		hasMeasurement = true;
	}
	
	hasNewScan = false;
}

bool MeasurePallete(gdrone_tools::Dist::Request &req, 
            gdrone_tools::Dist::Response &res)
{
	_MeasurePallete = true;
	while(!hasMeasurement)
	{
	}
	hasMeasurement = false;
	_MeasurePallete = false;
	res.result = measurement;
	std::cout<<"Result of MeasurePallete: "<<measurement<<std::endl;
	return true;
}

bool BackwardsPallete(gdrone_tools::Dist::Request &req, 
            gdrone_tools::Dist::Response &res)
{
	_BackwardsPallete = true;
	while(!hasMeasurement)
	{
	}
	hasMeasurement = false;
	_BackwardsPallete = false;
	res.result = measurement;

	std::cout<<"Result of BackwardsPallete: "<<measurement<<std::endl;
	return true;
}

bool ClosestPoint(gdrone_tools::Dist::Request &req, 
            gdrone_tools::Dist::Response &res)
{
	_ClosestPoint = true;
	while(!hasMeasurement)
	{
	}
	hasMeasurement = false;
	_ClosestPoint = false;
	res.result = measurement;

	std::cout<<"Result of ClosestPoint: "<<measurement<<std::endl;
	return true;
}

void laserReceived(const sensor_msgs::LaserScan::ConstPtr& scanH)
{
	
	newScan = *scanH;
	hasNewScan = true;
	analyseNewScan();
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "laser_distances");
	ros::NodeHandle nh("~"), g_nh;
	std::string scanTopic;
	// Resolve the scan topic where to puchish the assembled laser
	nh.param("topic", scanTopic, std::string("/agv1/base_scan"));
	
	laser_sub = nh.subscribe<sensor_msgs::LaserScan>(scanTopic, 1, laserReceived);
	
	ros::ServiceServer palleteMeasure= g_nh.advertiseService("MeasurePallete", &MeasurePallete);
	ros::ServiceServer palleteBackwards = g_nh.advertiseService("BackwardsPallete", &BackwardsPallete);
	ros::ServiceServer closestPoint = g_nh.advertiseService("ClosestPoint", &ClosestPoint);

	ros::Rate rate(60.0);
	//ros::Timer timer = nh.createTimer(ros::Duration(1)
	ros::MultiThreadedSpinner spinner(4); // Use 4 threads

	while (nh.ok())
	{
		/*if(hasNewScan)
		{
			//analyseNewScan();
			hasNewScan = false;
		}*/
		spinner.spin();
		rate.sleep();  
	}


	return 0;
}
