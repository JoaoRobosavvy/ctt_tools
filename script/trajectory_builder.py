#!/usr/bin/env python
import roslib; 
import rospy, rospkg
import copy
import os

	## @brief Callback called to load previous saved points
	def loadPointsCB(self, feedback):
		# Deletes all the loaded points
		if len(self.list_of_points) > 0:
			self.deleteAllPoints()
		
		try:
			file_points = open(self.points_file_path, 'r')
		except IOError, e:
			rospy.logerr( 'agvs_path_marker::loadPointsCB: File %s not found: %s'%(self.points_file_path, e))
			return
		
		num_of_loaded_points = 0
		# Reads and extracts the information of every line
		line = file_points.readline().replace('\n', '')
		while line != '':
			# line format = 'p1;p1->0.4;0.5;5.2;0.4'
			a = line.split(';')
			# a = ['p1', 'p1->0.4', '0.5', '0.4', '0.4'] // [ ID, DESCRIPTION, X, Y, SPEED]
			if len(a) == 5:
				
				new_point = PointPath(self.frame_id, a[0], a[1], speed = float(a[4]))
				new_point.pose.position.x = float(a[2])
				new_point.pose.position.y = float(a[3])
				
				rospy.loginfo('agvs_path_marker::loadPointsCB: Loading point %s at position %.2lf, %.2lf. speed =  %.2lf'%(a[0], new_point.pose.position.x, new_point.pose.position.y, new_point.speed))
				
				self.list_of_points.append(new_point)
				self.insert(new_point, new_point.processFeedback)
				self.menu_handler.apply( self, a[0])
				self.applyChanges()
				self.counter_points = self.counter_points + 1
				num_of_loaded_points = num_of_loaded_points + 1
			else:
				rospy.logerr('agvs_path_marker::loadPointsCB: Error processing line %s'%(line))		
				
			
			line = file_points.readline().replace('\n', '')
		
		file_points.close()
		
		rospy.loginfo('agvs_path_marker::loadPointsCB: Loaded %d points'%(num_of_loaded_points))	
		
if __name__=="__main__":
	rospy.init_node("agvs_path_builder")
	
	_name = rospy.get_name().replace('/','')
	
	arg_defaults = {
	  'frame_id': '/map',
	  'planner': 'purepursuit_planner'
	}
	
	args = {}
	
	for name in arg_defaults:
		try:
			if rospy.search_param(name): 
				args[name] = rospy.get_param('%s/%s'%(_name, name)) # Adding the name of the node, because the para has the namespace of the node
			else:
				args[name] = arg_defaults[name]
			#print name
		except rospy.ROSException, e:
			rospy.logerror('%s: %s'%(e, _name))
	
	server = PointPathManager(_name, frame_id = args['frame_id'], planner = args['planner'])
	
	rospy.spin()	
