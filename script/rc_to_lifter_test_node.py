#!/usr/bin/env python

import rospy
import gd_rc.msg
import gd_motors_brd.msg
#import std_msgs.msg

pub = None
#val1 = std_msgs.msg.String()
val1 = gd_motors_brd.msg.motor_system_variables()

#val1.data = 'c'
val1.position = 90
val1.k = 60

def subs_cb(msg):
    global pub, val1
    #~ for ch in msg.channel:
        #~ if ch.connected:
            #~ print ch.value
    # 'r'
    # 'g'
    # 'g'
    # 'f'
    # 'o'
    # 'c'
    
    #map position data with value of rc channel
    #val = map(val, 0, 1023, 0, 255);
    #long map(long x, long in_min, long in_max, long out_min, long out_max)
	#{
	#	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	#}
    
    val1.position=(msg.channel[5].value-(-25000))*(90-45)/(25000-(-25000))+45
    val1.k = 60
    #if msg.channel[5].connected:
        #if msg.channel[5].value < -20000:
            #val1.data = 'r'
        #elif msg.channel[5].value < -18000:
            #val1.data = 'g'
        #elif msg.channel[5].value < -14000:
            #val1.data = 'b'
        #elif msg.channel[5].value < -10000:
            #val1.data = 'f'
        #elif msg.channel[5].value < 0000:
            #val1.data = 'o'
        #else:
            #val1.data = 'c'
    

if __name__ == '__main__':
    rospy.init_node('node_name')
    
    rospy.Subscriber('RC_read', gd_rc.msg.rc_array,subs_cb)
    #pub = rospy.Publisher('effects', std_msgs.msg.String, queue_size=1)
    pub = rospy.Publisher('motor_vars', gd_motors_brd.msg.motor_system_variables, queue_size=1)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
    #while True:
        #rospy.loginfo(val1.data)
		print "here"
		rospy.loginfo(val1.position)
		pub.publish(val1)
		rate.sleep()
